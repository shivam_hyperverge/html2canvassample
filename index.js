let canvasGlobal = null
html2canvas(document.querySelector("#capture")).then(canvas => {
  document.body.appendChild(canvas);
  canvasGlobal = canvas;
  console.log(canvas.toDataURL("image/jpeg"));
});

const button = document.querySelector('button')




button.addEventListener('click', () => {
  let data = canvasGlobal.toDataURL("image/jpeg");
  data = data.replace('data:image/jpeg;base64,', '')
  console.log(data)
  downloadImage(data);
})

function downloadImage(data, filename = 'untitled.jpeg') {
    const a = document.createElement('a');
    a.href = 'data:application/octet-stream;headers=Content-Disposition%3A%20attachment%3B%20filename=' + filename + ';base64,' + data;
    a.download = filename;
    document.body.appendChild(a);
    a.click();
}


